var path = require('path'); 

module.exports = { 
    // mode: 'development', 
    mode: 'production', 
    entry: './js/index.js', 
    devtool: "source-map",
    output: { 
        filename: 'app.js', 
        path: path.resolve(__dirname, 'public_html/dist') 
    },
    module: {
	    rules: [
	      {
	        test: /\.js$/,
	        exclude: /node_modules/,
	        use: "babel-loader",
	        /*query: {
	          presets: ['es2015', 'react']
	        }*/
	      },
	      {
	        test: /\.css$/,
	        use: [
			    'style-loader',
			    'css-loader'
			  ],
	      },
	    ]
  }
}; 