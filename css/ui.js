import Styled from 'styled-components';

export const Button = Styled.button`
	&:disabled {
		cursor: not-allowed;
    	opacity: 0.6;
	}
	&:focus {
		outline: none;
	}
	&:hover:not(:disabled) {
		color: palevioletred;
		background: #fffed7;
	}
	cursor: pointer;
	padding: 0px 15px;
	line-height: 40px;
	background: #fff;
	border: 1px solid #fffed7;
	transition: all 0.3s; 
	font-size: inherit;
	font-family: inherit;
	${props => props.active && {
	    'border-color': 'palevioletred'}
	}
`

export 	const AddToDoButton = Button.extend`
	width: 50%;
    margin: 0 auto;
    display: block;
    margin-top: 10px;
    font-size: 18px;
    font-family: inherit;
    border: 1px solid #eee;
`
export const CheckBoxContainer = Styled.label `
	display: inline-block;
	position: relative;
	height: 25px;
	width: 25px;
	vertical-align: middle;
	cursor: pointer;
	font-size: 22px;
	user-select: none;
	margin-right: 10px;
`
export const CheckBox = Styled.input.attrs({
	type: 'checkbox',
	})`
	position: absolute;
	opacity: 0;
	cursor: pointer;
`
export const CheckBoxMark = Styled.span `
	position: absolute;
	top: 0;
	left: 0;
	height: 100%;
	width: 100%;
	background-color: #eee;
	transition: all 0.3s;
	&::after {
		position: absolute;
		display:none;
		content: '';
		left: 8px;
	    top: 3px;
	    width: 7px;
	    height: 12px;
	    border: solid white;
	    border-width: 0 3px 3px 0;
	    transform: rotate(45deg);
	}
	input:checked + &::after {
		display:block;
	}
	input:checked + &{
		background-color: palevioletred;
	}
`

