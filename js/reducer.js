export default function reducer(state = [], action) {

	const DEFAULT_TODO_LIST = [
		{
			id : 1,
			text: 'Написать ToDo лист',
			completed: true,
		},
		{
			id : 2,
			text: 'Отправить на проверку',
			completed: true
		},
		{
			id : 3,
			text: 'Пройти собеседование',
			completed: false
		},
		{
			id : 4,
			text: 'Прийти к соглашению на собеседовании',
			completed: false
		},
		{
			id : 5,
			text: 'Приступить к работе',
			completed: false
		},
	]
	
	const NAMESPACE = 'ToDoList';

	let getDefaultToDoList = () => {
		
		let ToDoList = ToDoListFromLocalStorage();
		if (ToDoList.length) {
			return ToDoList

		} else {
			changeLocalStorage('add', DEFAULT_TODO_LIST);
			return DEFAULT_TODO_LIST;
		}
	}

	let ToDoListFromLocalStorage = () => {
		let storage = localStorage.getItem(NAMESPACE);
		return (storage && JSON.parse(storage)) || [];
	}

	let changeLocalStorage = (action, data) =>{
		let storage = ToDoListFromLocalStorage();
		switch (action) {
			case 'add':
				if (Array.isArray(data)) {
					storage.push(...data);
				} else {
					storage.push(data);
				}
				console.log('добавил')
				localStorage.setItem(NAMESPACE, JSON.stringify(storage));
			break;

			case 'delete':
				let filteredStorage = storage.filter(note => note.id != data);
				localStorage.setItem(NAMESPACE, JSON.stringify(filteredStorage));
			break;

			case 'replace':
				localStorage.setItem(NAMESPACE, JSON.stringify(data));
			break;
		}
	}

	const INITIAL_STATE = {
		filter: '',
		hideCompleted: false,
		sortByField: 'id',
		sortDirection: 1,
		notes: getDefaultToDoList(),
	};








	switch (action.type) {
		case 'ADD_TODO':
			console.log('ADD_TODO')
			let newNote = {
				id: action.id,
				text: action.text,
				completed: false
			}
			let notes = [...state.notes, newNote]
			let newState = {
				...state, notes: notes
			}
			
			changeLocalStorage('add', newNote);
			/*let newState = [...state.notes, newNote]
			console.log(newState);
			return newState;*/
			return newState;
		break;
		/*case 'CHANGE_TODO_COMPLETE': 
			return state.map(
					ToDo => ToDo.id === action.id?
					{...ToDo, completed: action.completed}
					:
					ToDo
				);
		break;*/
		case 'SET_FILTER': 
			console.log('SET_FILTER');
			return {
				...state, filter: action.text
			};
		break;
		case 'CHANGE_HIDE_COMPLETED': 
			console.log('CHANGE_HIDE_COMPLETED');
			return {
				...state, hideCompleted: action.hideCompleted
			};
		break;
		case 'CHANGE_SORT': 
			console.log('CHANGE_SORT');
			let direction = state.sortByField === action.field && state.sortDirection === 1 ? -1 : 1;
			return {
				...state, 
				sortByField: action.field,
				sortDirection: direction
			};
		break;
		case 'TODO_DELETE': 
			{
				console.log('TODO_DELETE');
				let notes = state.notes.filter(note => note.id != action.id);
				changeLocalStorage('replace', notes); 
				return {
					...state, notes: notes
				};
			}
		break;
		case 'CHANGE_COMPLETED': 
			{
				console.log('CHANGE_COMPLETED');
				let notes = state.notes.map(note => 
						note.id === action.id ? {...note, completed : action.completed} : note
					)
				changeLocalStorage('replace', notes); 
				return {
					...state, 
					notes: notes
				}
			}
		break;
		case 'CHANGE_TEXT': 
			{
				console.log('CHANGE_TEXT');
				let notes = state.notes.map(note => 
						note.id === action.id ? {...note, text : action.text} : note
					);
				changeLocalStorage('replace', notes); 
				return {
					...state, 
					notes: notes
				};
			}
		break;
		default:
			console.log('default')
			// console.log(getDefaultToDoList())
			// return getDefaultToDoList();
			return INITIAL_STATE;
	}

}
