export const addToDo = text => ({
	type: 'ADD_TODO',
	id: Date.now(),
	text: text,
});
export const changeToDoComplete = (id, completed) => ({
	type: 'CHANGE_TODO_COMPLETE',
	id: id,
	completed: completed,
});
export const TODO_DELETE = (id) => ({
	type: 'TODO_DELETE',
	id: id,
});
export const CHANGE_COMPLETED = (id, completed) => ({
	type: 'CHANGE_COMPLETED',
	id: id,
	completed: completed,
});
export const CHANGE_TEXT = (id, text) => ({
	type: 'CHANGE_TEXT',
	id: id,
	text: text,
});
export const SET_FILTER = (text) => ({
	type: 'SET_FILTER',
	text: text,
});
export const CHANGE_HIDE_COMPLETED = (value) => ({
	type: 'CHANGE_HIDE_COMPLETED',
	hideCompleted: value,
});
export const CHANGE_SORT = (field) => ({
	type: 'CHANGE_SORT',
	field: field,
});



