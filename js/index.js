'use strict';
import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducer';
import store from './store';
import * as UI from '../css/ui.js';
import ToDoEditor from './components/ToDoEditor';
import ToDoList from './components/ToDoList';
import ToolBar from './components/ToolBar';


const Wrapper = styled.div`
	@import url(https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&subset=cyrillic-ext);
	width: 800px;
	max-width: 100%;
	display:block;
	margin:0 auto;
	font-size: 16px;
	font-family: 'Open Sans Condensed', sans-serif;
`;
const Title = styled.h1`
	font-size: 1.5em;
	text-align: center;
	color: palevioletred;
`;
const TopBlock = styled.div`
	padding: 0 10px;
	margin-bottom: 20px;
	&::after {
		content: '';
		display: block;
		clear: both;
	}
`;
const SortButtonsWrapper = styled.div`
	float: right;
`;
const TopBlockLabel = styled.label`
	line-height: 40px;
	margin-right: 10px;
`

class App extends React.Component {
	render() {
		return (
	    	<Wrapper>
	    		<Title>ToDo list</Title>
	    		<ToolBar/>
	    		<ToDoList/>
	    		<ToDoEditor/>
	    	</Wrapper>
		);
	}
}
ReactDOM.render(
	<Provider store = {store}>
		<App/>
	</Provider>
, document.getElementById('app'));