import React from 'react';
import styled from 'styled-components';
import * as UI from '../../css/ui.js';
import { connect } from 'react-redux';
import {SET_FILTER, CHANGE_HIDE_COMPLETED, CHANGE_SORT} from '../actions.js';

const ToolBarDOM = styled.div`
	padding: 0 10px;
	margin-bottom: 20px;
	&::after {
		content: '';
		display: block;
		clear: both;
	}
`;
const SortButtonsWrapper = styled.div`
	float: right;
`;
const ToolBarLabel = styled.label`
	line-height: 40px;
	margin-right: 10px;
	user-select: none;
`
@connect(state=> ({
		filter: state.filter,
		hideCompleted: state.hideCompleted,
		sortByField: state.sortByField,
		sortDirection: state.sortDirection
	}), {SET_FILTER, CHANGE_HIDE_COMPLETED, CHANGE_SORT})
export default class ToolBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = this.props;
	}
	render() {
		return (
	    	<ToolBarDOM>
	    			<ToolBarLabel>
	    				Поиск по тексту: 
	    				<input 
			    			// onChange={this.props.handleFilterChange}
			    			onChange={e => this.props.SET_FILTER(e.target.value.toLowerCase())}
			    			value={this.props.filter}
			    			style={{"margin-left":"10px"}}
			    		/>
	    			</ToolBarLabel>
    				<ToolBarLabel>
	    				<UI.CheckBoxContainer>
				    	 	<UI.CheckBox 
				    			onChange={e => this.props.CHANGE_HIDE_COMPLETED(e.target.checked)}
		    					checked={this.props.hideCompleted}/>
				    		<UI.CheckBoxMark/>
				    	</UI.CheckBoxContainer>
		    			Скрыть выполненные
    				</ToolBarLabel>
		    		
		    		<SortButtonsWrapper>
		    			<UI.Button 
		    				active={this.props.sortByField === 'id' ? true : false}
		    				onClick={e => this.props.CHANGE_SORT('id')}
			    			>
			    			Сортировать по дате &nbsp;
			    			{this.props.sortByField === 'id' && this.props.sortDirection === 1 ? "↓" : ''}
			    			{this.props.sortByField === 'id' && this.props.sortDirection === -1 ? "↑" : ''}
		    			</UI.Button>
		    			<UI.Button 
		    				onClick={e => this.props.CHANGE_SORT('text')}
		    				active={this.props.sortByField === 'text' ? true : false}
			    			>
		    				Сортировать по тексту &nbsp;
		    				{this.props.sortByField === 'text' && this.props.sortDirection === 1 ? "↓" : ''}
			    			{this.props.sortByField === 'text' && this.props.sortDirection === -1 ? "↑" : ''}
		    			</UI.Button>
		    		</SortButtonsWrapper>
	    		</ToolBarDOM>
		);
	}
}