import React from 'react';
import ToDoItem from './ToDoItem';
import Styled from 'styled-components';
import { connect } from 'react-redux';
import {changeToDoComplete, deleteToDo} from '../actions.js';

const RemoveButton = Styled.button`
	border: red;
	border-radius: 50%;
	background: #f00;
	color: #fff;
	position: absolute;
	right: 10px;
	&:hover {
	   background: palevioletred;
	}
`;
@connect(state=> ({state: state}))
export default class ToDoList extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		const notes = this.props.state.notes;
		let ToDoList = <p>Нет записей.</p>
		if (notes.length) {
			let sortedNotes = [].slice.call(notes).sort((a, b) => {
				let first = isNaN(a[this.props.state.sortByField]) ? a[this.props.state.sortByField].toLowerCase() : a[this.props.state.sortByField];
				let second = isNaN(b[this.props.state.sortByField]) ? b[this.props.state.sortByField].toLowerCase() : b[this.props.state.sortByField];
			    if (first === second) { return 0; }
			    return first > second ? this.props.state.sortDirection : this.props.state.sortDirection * -1;
			});
			let currentNotes = sortedNotes.filter((note) => {
				let completedApproved = false;
				if (this.props.state.hideCompleted) {
					completedApproved = (note.completed === false);
				} else {
					completedApproved = true;
				}
				return (note.text.toLowerCase().includes(this.props.state.filter) && completedApproved)
			});
			if (currentNotes.length) {
				ToDoList = currentNotes.map(
	    			note => <ToDoItem 
		    			key={note.id} 
		    			id={note.id} 
		    			text={note.text} 
		    			completed={note.completed}
		    			/>
	    		);
			}
			
		}
		return (
	    	<div className="ToDoList">
	    		{ToDoList}
	    	</div>
		);
	}
}