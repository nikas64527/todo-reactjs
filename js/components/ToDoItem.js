import React from 'react';
import Styled from 'styled-components';
import * as UI from '../../css/ui.js';
import { connect } from 'react-redux';
import {TODO_DELETE, CHANGE_COMPLETED, CHANGE_TEXT} from '../actions.js';

const RemoveButton = Styled.button`
	border: none;
	background: #f00;
	color: #fff;
	text-align: center;
	border:none;
	padding: 0;
	cursor: pointer;
	display: inline-block;
    vertical-align: middle;
    height: 25px;
    width: 25px;
    margin-left: 10px;
    line-height: 25px;
    font-size: 18px;
    opacity: 0.6;
    transition: all 0.3s;
	&:hover {
	   opacity: 1;
	}
`;
const ToDoItemDOM = Styled.div`
	&:hover {
	   background: #fffed7;
	}
	font-size: 0;
	position: relative;
	transition: all 0.3s;
	padding: 5px 10px;
	text-decoration: ${props => props.completed ? 'line-through' : 'none'}
`;
const ToDoItemInput = Styled.input.attrs({
	type: 'text',
	})`
	background: transparent;
	width: calc(100% - 70px);
	display: inline-block;
	vertical-align: middle;
	border: 1px solid transparent;
	box-sizing: border-box;
    height: 25px;
    line-height: 25px;
    padding: 0 10px;
    font-size: 16px;
    font-family: inherit;
	&:focus {
		outline: none;
		background: #fff;
		border: 1px solid #eee;
	}
	text-decoration: ${props => props.completed ? 'line-through' : 'none'}
`;
@connect(null, {TODO_DELETE, CHANGE_COMPLETED, CHANGE_TEXT})
export default class ToDoItem extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
	    	<ToDoItemDOM completed={this.props.completed}>
		    	<UI.CheckBoxContainer>
		    	 	<UI.CheckBox 
		    			checked={this.props.completed}
		    			onChange={e => this.props.CHANGE_COMPLETED(this.props.id, e.target.checked)}
		    			/>
		    		<UI.CheckBoxMark/>
		    	 </UI.CheckBoxContainer>
		    	<ToDoItemInput 
		    		onChange={e => this.props.CHANGE_TEXT(this.props.id, e.target.value)}
		    		value={this.props.text}
		    		selectionStart={this.props.text.length}
		    		completed={this.props.completed}
		    		/>
		    	
		    	<RemoveButton 
		    	onClick={e => this.props.TODO_DELETE(this.props.id)}
		    	>х
		    	</RemoveButton>
	    	</ToDoItemDOM>
		);
	}
}