import React from 'react';
import styled from 'styled-components';
import * as UI from '../../css/ui.js';
import {addToDo} from '../actions.js';
import { connect } from 'react-redux';

const EditorWrapper = styled.div`
  width: 100%;
  margin-top: 30px;
  padding: 0 10px;
`;
const EditorTextarea = styled.textarea`
  width: 50%;
  min-width: 200px;
  display:block;
  margin: 0 auto;
  padding: 10px;
  resize: vertical;
  box-sizing:border-box;
  min-height: 50px;
  font-family: inherit;
  font-size: inherit;
  &:focus {
  	outline: none;
  }
`;
const ENTER_KEY = 13;

// @connect(state=> ({ToDoList: state.notes}), {addToDo})
@connect(null, {addToDo})
export default class ToDoEditor extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	handleTextChange(e) {
		this.setState({
			text: e.target.value
		});
	}
	clearStateText() {
		this.setState({
			text: ''
		});
	}
	handleKeyDown(e) {
		/*(e.keyCode === ENTER_KEY)? console.log('нажали энтер'): false;
		console.log(e);*/
	}
	handleAddToDo = () => {
		this.props.addToDo(this.state.text);
		this.clearStateText();
	}
	render() {
		return (
	    	<EditorWrapper>
	    		<EditorTextarea
	    			placeholder="Что нужно сделать?"
	    			value={this.state.text}
	    			onChange={this.handleTextChange.bind(this)}
	    			onKeyDown={this.handleKeyDown}/>
	    		<UI.AddToDoButton 
		    		onClick={this.handleAddToDo} 
		    		disabled={!this.state.text}>Добавить
	    		</UI.AddToDoButton>
	    	</EditorWrapper>
		);
	}
}
